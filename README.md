# cinch-message_logger

* [Homepage](https://rubygems.org/gems/cinch-message_logger)
* [Documentation](http://rubydoc.info/gems/cinch-message_logger/frames)
* [Email](mailto:KitaitiMakoto at gmail.com)

## Description

Cinch plugin for recording messages by multiple formats

## Features

## Examples

    require 'cinch/message_logger'

## Requirements

## Install

    $ gem install cinch-message_logger

## Copyright

Copyright (c) 2018 KITAITI Makoto

See {file:COPYING.txt} for details.
