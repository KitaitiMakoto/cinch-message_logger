#
# Copyright (c) 2018 KITAITI Makoto (KitaitiMakoto at gmail.com)
#
# cinch-message_logger is free software: you can redistribute it and/or modify
# it under the terms of the GNU Lesser General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# cinch-message_logger is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Lesser General Public License for more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with cinch-message_logger.  If not, see <http://www.gnu.org/licenses/>.
#

module Cinch
  module MessageLogger
    # cinch-message_logger version
    VERSION = "0.1.0"
  end
end
