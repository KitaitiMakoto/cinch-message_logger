require 'helper'
require 'cinch/message_logger'

class TestCinch::MessageLogger < Test::Unit::TestCase

  def test_version
    version = Cinch::MessageLogger.const_get('VERSION')

    assert !version.empty?, 'should have a VERSION constant'
  end

end
